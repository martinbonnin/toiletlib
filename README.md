# A simple app to show the closest toilets in Paris 
 
It uses Paris [OpenData](https://data.ratp.fr/api/records/1.0/search/?dataset=sanisettesparis2011&rows=1000) to display the toilets on a map directly on your phone any time you need them ! 

It works offline so you can be sure to find a good seat even you don't have network.

It's written in kotlin and uses Room for storage and rxjava bindings so that the UI reacts to database changes

![Toiletlib in action](https://gitlab.com/martinbonnin/toiletlib/raw/master/screenshot.png)

# Design considerations

* I went for 2 models, one for the network and one for the local database. This was mainly motivated by the fact that the network model is not really easy to work with (deeply nested + lots of useless fields)
* One thing we could do is use AutoValue for the network model so that the json parsing code uses no reflection at all. That might save a few milliseconds at launch
* I tried to keep clear of Fragments so everything is a View.
* I used a bottom navigation for its simplicity and ubiquitousness
* The store is a key component as it refreshes data and stores it in the SQLLite db. Data is written to the DB and read everytime, which is slightly sub-optimal but doesn't really have any impact for such small data. If it were to become a problem, we could add a memory cache as a level 1 cache in addition to level 2 SQLLite cache.
* There is one unit test for the `mergeToiletLists` function and one functional test. More functional tests can be added as the app grows.
* the favorites are stored localy in the same SQLLite table as the main table data. This has the drawback that it has to be merged each time we update the data but it keeps the DB schema simple (no relations) 

# todo 

* allow to add to favorite favorite directly from the map using `GoogleMap.setInfoWindowAdapter` and not just from the list
* translate in french, make a nicer logo
* improve the launch animation so that there is no white at the launch of the app.
* more generally, add more animations. The refresh button is a good candidate to start with
* save the last known position so that we can center the map on this position when reopening the app
* a nice popup/onboarding before jumping at the user throat asking for location permissions
* maybe display more information ? (although I doubt people really care about the other data in the database)
* maybe add some connectivity notification so that we can disable the refresh button and also notify the user when offline
* allow to share and sort by distance
* mock the network and add more functional tests

