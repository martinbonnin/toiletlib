package toiletlib.mbonnin.net.toiletlib

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import org.hamcrest.Matchers.not
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    @Rule
    @JvmField
    val activityRule: ActivityTestRule<MainActivity> = ActivityTestRule(MainActivity::class.java)

    /**
     * a very simple test that just tests the bottom navigation
     */
    @Test
    fun useAppContext() {
        onView(withId(R.id.list)).check(matches(not(isDisplayed())))
        onView(withId(R.id.navigation_list)).perform(ViewActions.click())
        onView(withId(R.id.list)).check(matches(isDisplayed()))
    }
}
