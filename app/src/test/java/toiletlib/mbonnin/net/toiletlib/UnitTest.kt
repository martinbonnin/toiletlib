package toiletlib.mbonnin.net.toiletlib

import org.junit.Assert.assertArrayEquals
import org.junit.Test

class UnitTest {
    @Test
    fun merge_isCorrect0() {
        val oldList = listOf(
                Toilet("1", "a", null, 0.0, 0.0, 1, 0, 0, true),
                Toilet("2", "a", null, 0.0, 0.0, 1, 0, 0, false),
                Toilet("3", "a", null, 0.0, 0.0, 1, 0, 0, true),
                Toilet("4", "a", null, 0.0, 0.0, 1, 0, 0, true)
        )

        val newList = listOf(
                Toilet("1", "a", null, 0.0, 0.0, 1, 0, 0, false),
                Toilet("2", "a", null, 0.0, 0.0, 1, 0, 0, false),
                Toilet("4", "a", null, 0.0, 0.0, 1, 0, 0, false)
        )

        val expectedList = listOf(
                Toilet("1", "a", null, 0.0, 0.0, 1, 0, 0, true),
                Toilet("2", "a", null, 0.0, 0.0, 1, 0, 0, false),
                Toilet("4", "a", null, 0.0, 0.0, 1, 0, 0, true)
        )

        val mergedList = Store.mergeToiletLists(oldList, newList)

        assertArrayEquals(
                expectedList.toTypedArray(),
                mergedList.toTypedArray()
        )
    }

    @Test
    fun merge_isCorrect1() {
        val oldList = listOf(
                Toilet("1", "a", null, 0.0, 0.0, 1, 0, 0, true),
                Toilet("2", "a", null, 0.0, 0.0, 1, 0, 0, false),
                Toilet("4", "a", null, 0.0, 0.0, 1, 0, 0, true)
        )

        val newList = listOf(
                Toilet("1", "a", null, 0.0, 0.0, 1, 0, 0, false),
                Toilet("2", "a", null, 0.0, 0.0, 1, 0, 0, false),
                Toilet("5", "a", null, 0.0, 0.0, 1, 0, 0, false),
                Toilet("3", "a", null, 0.0, 0.0, 1, 0, 0, false),
                Toilet("4", "a", null, 0.0, 0.0, 1, 0, 0, false)
        )

        val expectedList = listOf(
                Toilet("1", "a", null, 0.0, 0.0, 1, 0, 0, true),
                Toilet("2", "a", null, 0.0, 0.0, 1, 0, 0, false),
                Toilet("5", "a", null, 0.0, 0.0, 1, 0, 0, false),
                Toilet("3", "a", null, 0.0, 0.0, 1, 0, 0, false),
                Toilet("4", "a", null, 0.0, 0.0, 1, 0, 0, true)
        ).sortedBy { it.id }

        val mergedList = Store.mergeToiletLists(oldList, newList)

        assertArrayEquals(
                expectedList.toTypedArray(),
                mergedList.toTypedArray()
        )
    }
}
