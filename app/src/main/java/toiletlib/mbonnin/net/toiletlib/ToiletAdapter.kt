package toiletlib.mbonnin.net.toiletlib

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.v7.widget.RecyclerView
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import java.util.*

class ToiletAdapter(val context: Context, val store: Store): RecyclerView.Adapter<ToiletAdapter.ToiletViewHolder>() {

    private lateinit var toiletList: List<Toilet>

    private val starFullDrawable: Drawable
    private val starBorderDrawable: Drawable

    init {
        starFullDrawable = context.resources.getDrawable(R.drawable.ic_star_black_24dp)
        starBorderDrawable = context.resources.getDrawable(R.drawable.ic_star_border_black_24dp)
    }

    fun setToiletList(toiletList: List<Toilet>) {
        this.toiletList = toiletList

        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ToiletAdapter.ToiletViewHolder, position: Int) {
        val toilet = toiletList[position]
        if (toilet.street_number != null) {
            holder.title.setText(toilet.street_number + " " + toilet.street)
        } else {
            holder.title.setText(toilet.street)
        }

        val snippet: String

        if (toilet.opens_at_millis >= 0) {
            val format = DateFormat.getTimeFormat(holder.itemView.context)

            snippet = format.format(Date(toilet.opens_at_millis.toLong())) + "-" + format.format(Date(toilet.closes_at_millis.toLong()))
        } else {
            snippet = holder.itemView.context.getString(R.string.open_24_24)
        }
        holder.subtitle.setText(snippet)

        setFavorite(holder.star, toilet.favorite)
        holder.star.setOnClickListener({v ->
            toilet.favorite = !toilet.favorite
            setFavorite(holder.star, toilet.favorite)

            store.updateToilet(toilet)
        })
    }

    override fun getItemCount(): Int {
        return toiletList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ToiletAdapter.ToiletViewHolder {
        return ToiletViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item, parent, false))
    }

    class ToiletViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val title: TextView = itemView.findViewById(R.id.title)
        val subtitle: TextView = itemView.findViewById(R.id.subtitle)
        val star: ImageView = itemView.findViewById(R.id.star)
    }

    private fun setFavorite(star: ImageView, favorite: Boolean) {
        star.setImageDrawable(if (favorite) starFullDrawable else starBorderDrawable)
    }
}