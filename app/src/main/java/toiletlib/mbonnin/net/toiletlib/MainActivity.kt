package toiletlib.mbonnin.net.toiletlib

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.ActivityCompat
import android.support.v4.graphics.drawable.DrawableCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.text.format.DateFormat
import android.view.Menu
import android.view.MenuItem.SHOW_AS_ACTION_ALWAYS
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.view.DefaultClusterRenderer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber
import java.util.*


class MainActivity : AppCompatActivity() {

    private val REQUEST_CODE_PERMISSION_FINE_LOCATION = 42
    private val store = Store(this)
    private lateinit var adapter: ToiletAdapter

    private var googleMap: GoogleMap? = null
    private var lastList: List<Toilet>? = null

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->

        map.visibility = if (item.itemId == R.id.navigation_map) VISIBLE else GONE
        list.visibility = if (item.itemId == R.id.navigation_list) VISIBLE else GONE

        return@OnNavigationItemSelectedListener true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        adapter = ToiletAdapter(this, store)

        Timber.plant(Timber.DebugTree())

        map.visibility = VISIBLE
        list.visibility = GONE

        map.onCreate(savedInstanceState)

        store.refresh()

        list.layoutManager = LinearLayoutManager(this)
        list.adapter = adapter

        map.getMapAsync(this::mapCallback)
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        disposable = store.observe().observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onNewData, this::onError)
    }

    @SuppressLint("NewApi")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == REQUEST_CODE_PERMISSION_FINE_LOCATION) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PERMISSION_GRANTED) {
                googleMap?.setMyLocationEnabled(true)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val menuItem = menu.add("refresh")

        val drawable = resources.getDrawable(R.drawable.ic_refresh_black_24dp).mutate()
        DrawableCompat.setTint(drawable, Color.WHITE)
        menuItem.icon = drawable
        menuItem.setShowAsAction(SHOW_AS_ACTION_ALWAYS)
        menuItem.setOnMenuItemClickListener {
            store.refresh()
            Toast.makeText(this, getString(R.string.updating_data), Toast.LENGTH_SHORT).show()
            false
        }

        return true
    }

    private var disposable: Disposable? = null

    private var clusterManager: ClusterManager<MyItem>? = null

    @SuppressLint("MissingPermission")
    fun mapCallback(googleMap: GoogleMap) {
        this.googleMap = googleMap

        val theFamily = LatLng(48.8525146,2.3646884)

        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(theFamily, 16f))
        googleMap.getUiSettings().setMapToolbarEnabled(false);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            googleMap.setMyLocationEnabled(true)
        } else {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PERMISSION_GRANTED) {
                googleMap.setMyLocationEnabled(true)
            } else {
                ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        REQUEST_CODE_PERMISSION_FINE_LOCATION)
            }
        }

        // add a clusterManager to handle small zoom levels correctly
        clusterManager = ClusterManager<MyItem>(this, googleMap)
        clusterManager?.renderer = object: DefaultClusterRenderer<MyItem>(this, googleMap, clusterManager!!) {
            override fun onBeforeClusterItemRendered(item: MyItem?, markerOptions: MarkerOptions?) {
                super.onBeforeClusterItemRendered(item, markerOptions)
                // 207 is the hue of toilet blue !
                markerOptions?.icon(BitmapDescriptorFactory.defaultMarker(207f))
            }
        }

        googleMap.setOnCameraIdleListener(clusterManager!!)
        googleMap.setOnMarkerClickListener(clusterManager!!)

        if (lastList != null) {
            updateMarkers(lastList!!)
        }
    }

    fun onNewData(toiletList: List<Toilet>) {
        this.lastList = toiletList
        adapter.setToiletList(toiletList)

        if (googleMap != null) {
            updateMarkers(toiletList)
        }
    }

    private fun updateMarkers(toiletList: List<Toilet>) {
        Timber.d("updateMarkers: " + toiletList.size + " toilets")
        toiletList.forEach {
            val snippet: String

            if (it.opens_at_millis >= 0) {
                val format = DateFormat.getTimeFormat(this)

                snippet = getString(R.string.open_from_to, format.format(Date(it.opens_at_millis.toLong())), format.format(Date(it.closes_at_millis.toLong())))
            } else {
                snippet = getString(R.string.open_24_24)
            }

            clusterManager!!.addItem(MyItem(it.latitude,
                    it.longitude,
                    (it.street_number?:"") + " " + it.street,
                    snippet))
        }

        clusterManager!!.cluster()
    }

    fun onError(throwable: Throwable) {
        Toast.makeText(this, getString(R.string.store_error), Toast.LENGTH_SHORT).show()
    }

    override fun onStart() {
        super.onStart()
        map.onStart()
    }

    override fun onResume() {
        super.onResume()
        map.onResume()
    }

    override fun onPause() {
        super.onPause()
        map.onPause()
    }

    override fun onStop() {
        super.onStop()
        map.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        map.onDestroy()

        disposable?.dispose()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        map.onSaveInstanceState(outState)
    }

    override fun onLowMemory() {
        super.onLowMemory()
        map.onLowMemory()
    }

}
