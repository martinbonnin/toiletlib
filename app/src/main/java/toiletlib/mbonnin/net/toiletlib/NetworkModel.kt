package toiletlib.mbonnin.net.toiletlib

data class NetworkResponse(val records: List<NetworkRecord>)
data class NetworkRecord(val recordid: String,
                         val fields: NetworkRecordFields)

data class NetworkRecordFields(val nom_voie: String,
                               val geom_x_y: List<Double>,
                               val numero_voie: String?,
                               val arrondissement: String,
                               val horaires_ouverture: String)
