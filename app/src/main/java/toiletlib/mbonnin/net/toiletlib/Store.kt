package toiletlib.mbonnin.net.toiletlib

import android.arch.persistence.room.*
import android.content.Context
import android.widget.Toast
import com.squareup.moshi.KotlinJsonAdapterFactory
import com.squareup.moshi.Moshi
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.Request
import okio.Okio
import timber.log.Timber
import java.io.IOException
import java.util.regex.Pattern


class Store(val context: Context) {
    val db = Room.databaseBuilder(context, AppDatabase::class.java, "db").build()
    // run all the db operation on the same thread
    val scheduler = Schedulers.single()
    val moshi = Moshi.Builder()
            // Add any other JsonAdapter factories.
            .add(KotlinJsonAdapterFactory())
            .build()
    val jsonAdapter = moshi.adapter(NetworkResponse::class.java)

    fun refresh() {
        Maybe.fromCallable { httpGet() }
                .subscribeOn(Schedulers.io())
                .doOnSuccess {
                    updateDb(it)
                }
                .subscribeOn(scheduler)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Toast.makeText(context, context.getString(R.string.data_updated), Toast.LENGTH_SHORT).show()
                    Timber.d("yoohoo")
                }, {
                    Timber.d(it)
                })
    }

    private fun updateDb(toiletList: List<Toilet>) {
        val oldList = db.toiletDao().all()

        val mergedList = mergeToiletLists(oldList, toiletList)

        db.beginTransaction()
        db.toiletDao().deleteAll()
        db.toiletDao().insertAll(mergedList)
        db.setTransactionSuccessful()
        db.endTransaction()
    }

    fun observe(): Flowable<List<Toilet>> {
        return db.toiletDao()
                .observe()
                .subscribeOn(scheduler)
    }

    fun httpGet(): List<Toilet> {
        val client = OkHttpClient()

        val call = client.newCall(Request.Builder()
                .get()
                .url("https://data.ratp.fr/api/records/1.0/search/?dataset=sanisettesparis2011&rows=1000")
                .build())

        val response = call.execute()
        if (!response.isSuccessful)
            throw IOException("bad response: " + response.code())
        val body = response.body()
        if (body == null) {
            throw IOException("empty body response: " + response.code())
        }

        body.byteStream().use {
            val networkResponse = jsonAdapter.fromJson(Okio.buffer(Okio.source(it)))
            val list = mutableListOf<Toilet>()

            networkResponse?.records?.forEach {
                val opens_at_millis: Int
                val closes_at_millis: Int


                val matcher = OPENING_TIMES_PATTERN.matcher(it.fields.horaires_ouverture)

                if (matcher.matches()) {
                    opens_at_millis = matcher.group(1).toInt() * 3600 * 1000
                    closes_at_millis = matcher.group(2).toInt() * 3600 * 1000
                } else {
                    opens_at_millis = -1
                    closes_at_millis = -1
                }

                list.add(Toilet(
                        id = it.recordid,
                        street = it.fields.nom_voie,
                        street_number = it.fields.numero_voie,
                        latitude = it.fields.geom_x_y[0],
                        longitude = it.fields.geom_x_y[1],
                        district = it.fields.arrondissement.toInt(),
                        opens_at_millis = opens_at_millis,
                        closes_at_millis = closes_at_millis,
                        favorite = false
                ))
            }

            return list
        }
    }

    companion object {
        val OPENING_TIMES_PATTERN = Pattern.compile("([0-9]*) h - ([0-9]*) h")

        /**
         * merge old into new, copying the value of favorite from old to new
         *
         * this works by first sorting the two lists and then
         */
        fun mergeToiletLists(old: List<Toilet>, new: List<Toilet>): List<Toilet> {
            val oldList = old.sortedBy { it.id }
            val newList = new.sortedBy { it.id }

            var j = 0
            new_loop@ for (i in 0 until newList.size) {
                old_loop@ while (true) {
                    if (j >= oldList.size) {
                        // there are no old values to look for anymore
                        break@new_loop
                    }

                    if (newList[i].id > oldList[j].id) {
                        // this old value is not present anymore, go to next old_value
                        j++
                    } else if (newList[i].id == oldList[j].id) {
                        newList[i].favorite = oldList[j].favorite
                        j++
                        // this is a match, copy favorite and got to next new value
                        break@old_loop
                    } else {
                        // this new value did not exist before, go to next new value
                        break@old_loop
                    }
                }
            }

            return newList
        }
    }

    fun updateToilet(toilet: Toilet) {
        Completable.fromAction({db.toiletDao().update(toilet)})
                .subscribeOn(scheduler)
                .subscribe()
    }
}

@Entity
data class Toilet(
        @PrimaryKey
        var id: String,
        var street: String,
        var street_number: String?,
        var latitude: Double,
        var longitude: Double,
        var district: Int,
        var opens_at_millis: Int,
        var closes_at_millis: Int,
        var favorite: Boolean
)

@Dao
interface ToiletDao {
    @Query("SELECT * FROM toilet")
    fun observe(): Flowable<List<Toilet>>

    @Query("SELECT * FROM toilet")
    fun all(): List<Toilet>

    @Insert
    fun insertAll(toilets: List<Toilet>)

    @Update
    fun update(toilet: Toilet)

    @Query("DELETE FROM toilet")
    fun deleteAll()
}

@Database(entities = arrayOf(Toilet::class), version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun toiletDao(): ToiletDao
}