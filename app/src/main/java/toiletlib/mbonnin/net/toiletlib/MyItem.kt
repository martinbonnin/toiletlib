package toiletlib.mbonnin.net.toiletlib

import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem


class MyItem(lat: Double, lng: Double, val title_: String, val snippet_: String) : ClusterItem {

    val latLng = LatLng(lat, lng)

    override fun getPosition(): LatLng {
        return latLng
    }

    override fun getTitle(): String {
        return title_
    }

    override fun getSnippet(): String {
        return snippet_
    }
}